import curses
import time
import sys

TEXTE_ROUGE = "\033[38;5;124m"
RESET_COULEUR = "\033[0m"

TAILLE = 6
TEMPO = 0.1
PAUSE = 1
# heuristiques :
## 1 reine par ligne
## 1 reine par colonne
## les prises étant symétriques, vérifier vers le haut suffit

class ExceptionPlacement(Exception):
    def __init__(self, delta_colonne, ligne_haute, colonne_haute, ligne_basse):
        Exception.__init__(self, "Erreur")
        self.delta_colonne = delta_colonne
        self.ligne_haute = ligne_haute
        self.colonne_haute = colonne_haute
        self.ligne_basse = ligne_basse
        
class Echiquier:
    def __init__(self, taille, ecran):
        self.taille = taille
        self.ecran = ecran
        self.plateau = []
        for _ in range(taille):
            self.plateau.append([" "] * taille)
        self.colonnesUtilisees = set()

    def __str__(self):
        separation_haute = "┌" + "─┬" * (self.taille - 1) + "─┐\n"
        separation_milieu = "├" + "─┼" * (self.taille - 1) + "─┤\n"
        separation_basse = "└" + "─┴" * (self.taille - 1) + "─┘\n"
        separation = "-" * (2 * self.taille + 1)
        retour = separation_haute
        num_ligne = 0
        for ligne in self.plateau:
            for case in ligne:
                retour += "│" + case
            retour += "│" + "\n"
            num_ligne += 1
            if num_ligne < len(self.plateau): 
                retour += separation_milieu
            else:
                retour += separation_basse
        return retour

    def placerReine(self, num_reine = 0):
        if num_reine == self.taille:
            # on a fini !
            return self
        for colonne in range(self.taille):
            if colonne in self.colonnesUtilisees:
                continue
            nouvel_echiquier = self.copier()
            try:
                nouvel_echiquier.jouerReine(num_reine, colonne)
                nouvel_echiquier.afficher()
            except ExceptionPlacement as ex:
                nouvel_echiquier.afficher(ex)
                self.ecran.addstr(curses.LINES-1, 0, "ERREUR", curses.A_REVERSE)
                self.ecran.refresh()
                time.sleep(PAUSE)
                self.ecran.addstr(curses.LINES-1, 0, "      ")
                continue
            solution = nouvel_echiquier.placerReine(num_reine+1)
            if solution is not None:
                return solution
        return None

    def jouerReine(self, num_ligne, num_colonne):
        self.plateau[num_ligne][num_colonne] = "R"
        self.verifierNordOuest(num_ligne, num_colonne)
        self.verifierNordEst(num_ligne, num_colonne)
        self.colonnesUtilisees.add(num_colonne)

    def copier(self):
        copie = Echiquier(self.taille, self.ecran)
        copie.colonnesUtilisees = set(self.colonnesUtilisees)
        for num_ligne in range(self.taille):
            for num_colonne in range(self.taille):
                copie.plateau[num_ligne][num_colonne] = self.plateau[num_ligne][num_colonne]
        return copie

    def verifierNordOuest(self, num_ligne, num_colonne):
        ligne_depart = num_ligne
        colonne_depart = num_colonne
        while num_ligne > 0 and num_colonne > 0:
            num_ligne -= 1
            num_colonne -= 1
            if self.plateau[num_ligne][num_colonne] == "R":
                raise ExceptionPlacement(1, num_ligne, num_colonne, ligne_depart)

    def verifierNordEst(self, num_ligne, num_colonne):
        ligne_depart = num_ligne
        colonne_depart = num_colonne
        while num_ligne > 0 and num_colonne < self.taille - 1:
            num_ligne -= 1
            num_colonne += 1
            if self.plateau[num_ligne][num_colonne] == "R":
                raise ExceptionPlacement(-1, num_ligne, num_colonne, ligne_depart)

    def afficher(self, erreur = None):
        affichage = str(self)
        lignes = affichage.split("\n")
        for i in range(len(lignes)):
            if i % 2 == 0:
                # on écarte les lignes de tirets
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, 0, lignes[i])
                continue
            num_ligne = i // 2 
            affichage_ligne = lignes[i]
            if erreur is not None:
                indice = (num_ligne - erreur.ligne_haute)*erreur.delta_colonne*2 + (1 + 2 * erreur.colonne_haute)
            if erreur is not None and (num_ligne == erreur.ligne_haute or num_ligne == erreur.ligne_basse):
                debut = affichage_ligne[:indice]
                fin = affichage_ligne[indice+1:]
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, 0, debut)
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, len(debut), "R", curses.color_pair(1))
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, len(debut)+1, fin)
            elif erreur is not None and (erreur.ligne_haute < num_ligne < erreur.ligne_basse):
                debut = affichage_ligne[:indice]
                fin = affichage_ligne[indice+1:]
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, 0, debut)
                if erreur.delta_colonne == 1:
                    self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, len(debut), "\\", curses.color_pair(1))
                else:
                    self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, len(debut), "/", curses.color_pair(1))                    
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, len(debut)+1, fin)            
            else:
                self.ecran.addstr(curses.LINES-2*self.taille-1+i-1, 0, affichage_ligne)
        self.ecran.refresh()
        time.sleep(TEMPO)

def main():
    global TAILLE, TEMPO, PAUSE
    try:
        ecran = initCurses()
        if len(sys.argv) > 1:
            TAILLE = int(sys.argv[1])
            assert TAILLE*2+1 < curses.LINES, "terminal trop petit pour la taille d'échiquier choisie"
        if len(sys.argv) > 2:
            TEMPO = float(sys.argv[2])
        if len(sys.argv) > 3:
            PAUSE = float(sys.argv[3])
        echiquier = Echiquier(TAILLE, ecran)
        echiquier.placerReine()
        ecran.getch()
    finally:
        endCurses(ecran)

def initCurses():
    stdscr= curses.initscr()
    curses.start_color()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)
    return stdscr

def endCurses(stdscr):
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()

if __name__ == "__main__":
    if len(sys.argv) > 1 and (sys.argv[1] == "--help" or sys.argv[1] == "-h"):
        print(sys.argv[0], "[taille [tempo [pause]]]")
        print("\nProgramme ayant pour but de résoudre le 'problème des n reines' : il s'agit, sur une échiquier de n x n cases, de caser n reines (du jeu d'échec) de façon à ce qu'aucune reine ne soit en prise avec une autre.\n")
        print("taille : taille de la grille")
        print("tempo : pause (en secondes) entre chaque placement de reine")
        print("pause : pause (en secondes) en cas d'erreur")
    else:
        main()
