# n-reines

## Introduction
Programme permettant de résoudre le problème des n-reines : il s'agit de placer n reine sur un échiquier de taille n x n sans que 2 reine ne soient en prise l'une avec l'autre

## Paramètres
On peut paramétrer la taille de l'échiquier, la pause entre 2 placements de reine, la pause après un placement erroné.

`python3 nreines.py --help` pour plus d'info.